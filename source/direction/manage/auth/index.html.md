---
layout: markdown_page
title: "Category Vision - Authorization and Authentication"
---

- TOC
{:toc}

## Authorization and Authentication: Introduction

Thanks for visiting the strategy page for Authentication and Authorization in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/1983) for this category.

Authentication and authorization are critical, foundational elements to keeping resources secure but accessible. This is of particular importance for large enterprises or companies operating in regulated environments; for an authentication strategy to function, it must be secure and scalable. Onboarding/offboarding new employees should be automatable and not allow unauthorized users to access sensitive information.

Furthermore, security strategies have evolved from one of assumed trust within a trusted network. Historically, most organizations assume that authenticated users coming from a trusted source are allowed to access resources - those outside, if not on a VPN, cannot. However, security perimeters have become increasingly porous and an assumed trust model begins to break down with more services shifting to cloud and more employees BYODing or working remotely. 

While GitLab's supported strategies meet most expectations (especially outside of the enterprise), our strategy is to aggressively invest in further improvements to authentication and authorization, with a particular focus on SAML.

### Target audience and experience

The primary audience for future effort are administrators in medium to large enterprises. These are privileged, sophisticated users in companies managing employee identities with a single source of truth; this may be a series of LDAP servers or an IdAAS service like Okta. Automation matters - we should minimize the amount of manual work needed to onboard/offboard an employee and be able to assign permissions automatically - but the must-have is security, especially in sensitive environments operating under regulatory scrutiny. We need to strike a balance between being overly permissive (e.g. unintentionally allowing access to an offboarded employee) and overly restrictive (e.g. getting in the way of a developer's workflow and annoying them with reauthentication requests).

## What's Next and Why

Our primary focus for this category is [maximizing the availability](https://gitlab.com/groups/gitlab-org/-/epics/1986) and capabilities of SAML SSO in GitLab. We've invested and extended the capabilities offered by GitLab's SAML SSO for Groups - particularly important for users using GitLab.com - with improvements like [SCIM](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html#scim-provisioning-using-saml-sso-for-groups-silver-only), [SSO enforcement](https://docs.gitlab.com/ee/user/group/saml_sso/#sso-enforcement), and [group-managed accounts](https://docs.gitlab.com/ee/user/group/saml_sso/#group-managed-accounts). We'd like to ensure that we maximize the stability and availability of these features by investing in technical debt and bug fixes. 

You can see more details on our roadmap in our [Maturity Plan](#maturity-plan) below.

### How you can help

As with any category in GitLab, it's dependent on your ongoing feedback and contributions. Here's how you can help:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/1983).
   * We're especially interested in your experience if you're a large organization that's invested in centralized identity management and have suggestions on how GitLab could better support your needs.
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=authentication&label_name[]=Accepting%20merge%20requests)!

## Maturity Plan

This category is currently **Complete**. The next step in our maturity plan is achieving a **Lovable** state. 
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
* Note that a `Complete` state does not mean a category is "finished" and is no longer a priority. Even categories that are considered `Lovable` require continued investment.

While authentication and authorization in GitLab has a sufficient feature set to be competitive, we see significant opportunity to elevate our capabilities - especially for the enterprise. You can see the current scope of work to achieve this state in [the corresponding maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1964). Our priorities, in order of importance:

* [SAML SSO availability](https://gitlab.com/groups/gitlab-org/-/epics/1785). As mentioned above, we've improved the capabilities of [SAML SSO](https://docs.gitlab.com/ee/user/group/saml_sso/) significantly in CY2019. Before we extend this feature further, we're going to invest in quality and availability to ensure that we're able to iterate on a stable, generally available feature.
  * [Provider specific SCIM support](https://gitlab.com/groups/gitlab-org/-/epics/1900). In parallel, we'd also like to iterate on SCIM and ensure that it's tested and working for a variety of common identity providers. Our first iteration of this feature was tested with Azure AD, but we want SCIM to work well with a host of providers - and document/record the configuration experience for each.
  * [Parity with self-managed](https://gitlab.com/gitlab-org/gitlab/issues/12823). The last step in our availability efforts should be ensuring that self-managed instances who are already invested in instance-level authentication strategies can benefit from SAML SSO and SCIM.

Once we can iterate on a stable set of capabilities, we should move our attention to extending the depth of our solutions in a number of key areas: 

* [Workspaces](https://gitlab.com/groups/gitlab-org/-/epics/1606). A major challenge for enterprises looking to adopt GitLab.com is a lack of control and oversight over user activity. GitLab.com is a large self-managed Ultimate instance with no administrators outside of GitLab, Inc. - restricting a large amount of control away from enterprises who would like to have tight control over their organization's activity on GitLab.com. We want to solve this by allowing organizations to namespace a section of a GitLab instance away from the rest of the instance at large, enabling local administration and reducing risk.
* [Extending SAML SSO](https://gitlab.com/groups/gitlab-org/-/epics/1986) into improved enforcement and authorization. Comprehensive enforcement (including API and Git activity) and automatically managing permissions through an identity provider are both heavily demanded improvements from enterprise customers.
* [Unify and scope tokens](https://gitlab.com/groups/gitlab-org/-/epics/637). Tokens are fragmented across a number of implementations in GitLab. We'd like to unify these behind a single approach and allow for flexible scoping to carefully control what a token can do and which resources it can interact with. 
