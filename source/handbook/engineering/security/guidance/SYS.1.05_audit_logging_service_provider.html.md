---
layout: markdown_page
title: "SYS.1.05 - Audit Logging: Service Provider Logging Requirements"
---
 
## On this page
{:.no_toc}
 
- TOC
{:toc}
 
# SYS.1.05 - Audit Logging: Service Provider Logging Requirements
 
## Control Statement

GitLab establishes unique logging and audit trails for each entity's cardholder data environment and complies with the following:
* logs are enabled for third-party applications
* logs are active by default
* logs are available for review by and communicated to the owning entity
 
## Context

Logging is the foundation for constant monitoring and active alerting.
 
## Scope

Not applicable to GitLab.

## Ownership

N/A
 
## Guidance

customers.gitlab.com, the subscription portal of GitLab, is not a hosting provider or provides shared hosting environments for multiple clients on the same server.
 
## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Audit Logging: Service Provider Logging Requirements issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/908) . 
 
### Policy Reference

N/A
 
## Framework Mapping

PCI
   * A.1
   * A.1.3
   * A.1.4
