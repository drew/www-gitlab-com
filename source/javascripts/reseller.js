(function() {
  var resellerLogo = document.getElementById('reseller-logo');
  if (resellerLogo.width >= (resellerLogo.height * 2)) {
    resellerLogo.style.width = '420px';
  } else {
    resellerLogo.style.width = '260px';
  }
})();
